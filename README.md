
Travaux pratiques : tests unitaires
===================================

Mise en place
-------------

Installer Jest :

```bash
npm install -D jest @types/jest 
yarn add -D jest @types/jest
```

Ajouter un script dans le `package.json` :

```js
{
    "scripts": {
        // ...
        "test": "jest"
    }  
}
```

Lancer les tests

Un premier test
---------------

But : tester la méthode String.indexOf.

Créer un fichier de test :

- fichier dans les sources avec l'extenstion `.spec.js`
- ou un fichier `.js` dans un dossier `__tests__` à la racine du projet

Écrire un test :

- une méthode test avec un nom et une fonction qui contient le code à éxecuter
- convention : Arrange / Act / Assert

Intermède #1
------------

Mettre en pratique sur votre projet tutoré

Un second test
--------------

- test unitaire = unité de code
- en général : un fichier / classe et on teste machinalement
- ennuyeux, tests peu lisibles
- approche par fonctionnalité
- mieux organisé, plus lisible = documentation vivante

Écrire le test d'ajout d'un article dans le panier

- `__tests__/cart/add-item.js`

Utiliser les mocks
------------------

- pourquoi : ne pas dépendre d'éléments extérieurs dans les TU
- exemple : BDD, API, FS
- lancer une BDD pour des TU est lent et peu maintenable
- appeler une API tierce n'est pas toujours possible
- on veut pouvoir simuler les cas d'erreurs

Écrire le test de paiement du panier

- `__tests__/cart/pay.js`  

Intermède #2
------------

Mettre en pratique sur votre projet tutoré

Mesurer la couverture des tests
-------------------------------

- tracer le code de production exécuté lors des tests
- permet de détecter le code non couvert par les tests

Ajouter un script dans le `package.json` :

```js
{
    "scripts": {
        // ...
        "test:coverage": "jest --coverage"
    } 
}
```

Lancer les tests avec le coverage

- génère un rapport
- également visible depuis l'IDE
- couvert ne veux pas dire testé
- 100% est impossible, 80% est la limite traditionnelle

Intermède #3
------------

Mettre en pratique sur le projet tutoré

Tester une interface graphique
==============================

Méthode des "snapshots"
-----------------------

- Jest permet de générer des snapshots des composants
- il s'agit d'une sauvegarde sous forme de fichier du rendu
- le premier test génère et sauvegarde le snapshot pour l'utiliser comme référence
- les tests suivants génèrent le snapshot et le compare au snapshot de référence
- si le nouveau snapshot est différent, le test échoue et affiche un "diff"
- si on juge ce nouveau snapshot comme correct, on peut le sauvegarder comme nouveau snapshot de référence

Installer react-test-renderer :

```bash
npm install --save-dev react-test-renderer
yarn add --dev react-test-renderer
```

Tester le composant Hero en utilisant la méthode des snapshots.

```js
import renderer from 'react-test-renderer';

test("test name", () => {
    // Arrange

    // Act
    const snapshot = renderer.create(
        // code JSX du composant à tester
    ).toJSON();

    // Assert
    expect(snapshot).toMatchSnapshot();
});
```

Méthode des attributs "data-test"
---------------------------------

- pour découpler le test du html, on va placer des attributs spécifiques
- par convention on utilisera le préfixe `data-testid`

Installer [React Testing Library](https://testing-library.com/docs/react-testing-library/api) :

```bash
npm install --save-dev @testing-library/react
yarn add --dev @testing-library/react
```

Tester le composant Hero en utilisant la méthode des attributs de test.

```js
import { render } from '@testing-library/react';

test("test name", () => {
    // Arrange

    // Act
    const { getByTestId } = render(
        // code JSX du composant à tester
    );

    // Assert
    expect(getByTestId("some-value")).toBeDefined();
});
```

- les deux approches permettent le même résultat
- la seconde approche est plus résiliente et plus expressive

Tester le composant Catalog : tester le nombre d'éléments et le click sur un bouton

- le clic sur le bouton n'est pas testable avec un snapshot

Notions avancées
================

Les tests paramétrés
--------------------

- permettent d'éviter de la duplication
- attention à ne pas réduire la lisibilité

Reprendre le test sur String.indexOf et utiliser [test.each](https://jestjs.io/docs/api#testeachtablename-fn-timeout) pour ne garder qu'un seul test.

```js
test.each`
    input      | expected
    ${"foo"}   | ${3}
    ${"lorem"} | ${5}
`("should return $expected when given $input", ({input, expected}) => {
    // Arrange
    // Act
    // Assert
})
```

Les hooks Before/After
----------------------

- permettent d'exécuter du code avant/après chaque test ou suite de test
- permettent de factoriser le code commun des tests
- attention à l'isolation des tests
- préférer des factories :
  - les factories sont plus lisibles
  - les factories poussent à mettre en place des conventions
  - les factories sont utilisables dans toutes les suites de tests

Reprendre un test précédent et mutualiser la génération des Items avec les hooks puis avec des factories.

Le property-based testing
=========================

- semblable au tests paramétrés mais avec des valeurs aléatoires
- évite le biais du développeur : l'utilisation de valeurs trop simples ou dont on est sûr

Installer `fast-check` :

```bash
npm install --dev fast-check
yarn add -D fast-check
```

Aller dans le test du montant total et utiliser la bibliothèque pour améliorer les tests.

```js
import * as fc from "fast-check";

test("test name", () => {
    fc.assert( // déclaration du property-based test
        fc.property( // déclaration des valeurs à générer
            fc.integer({ min: 0 }), // exemple : un entier positif
            fc.string(), // et un chaine de caractères
            (someInt, someText) => { // implémentation du test avec le AAA 

                // Arrange 
                // Act
                // Assert

            }
        )
    );
});
```

Tester la fonctionnalité de calcul des frais d'envoi ("Shipment").

Le Test Driven Development
==========================

- il s'agit d'écrire les tests avant le code de production
- les tests vont naturellement décrires les fonctionnalités
- on écrit des tests systématiquement

Règles de bases du TDD
----------------------

- on n'implémente pas de fonctionnalité sans avoir un test "rouge" pour cette fonctionnalité
- on ne fait pas de refactoring tant qu'un test est "rouge"

Le cycle "Red > Green > Refactor"
---------------------------------

1. On crée un test qui valide la fonctionnalité. Pendant cette phase, on implémente uniquement le strict nécessaire (créer des classes, méthodes) et on retourne uniquement des valeurs par défaut. Le test doit compiler mais échouer ("test rouge") : c'est la preuve que la fonctionnalité n'est pas implémentée.
2. On implémente la fonctionnalité car le test est rouge. On écrit le code le plus simple possible, on veut juste que ça marche. Le test doit réussir ("test vert").
3. On peut faire du refactoring si besoin car le test est vert.

Attention lors de la troisième étape. Lorsqu'on effectue un refactoring, on peut être amené à faire des abstraction et des choix de design. Si ce sont de mauvais choix, le code sera rendu inutilement complexe et dur à faire évoluer ensuite. Si vous avez un doute, laissez-vous un peu de temps et continuez à faire quelques cycles Red > Green pour voir si votre idée se concrétise ou non.

Mise en pratique
----------------

Pour Lilly, nous voulons ajouter une fonctionnalité de coupon. Elle pourra créer des coupons et les distribuer à sa communauté sur Twitch.  
Les coupons sont des chaines de caractères à entrer dans le panier pour obtenir une réduction. Il en existe deux types, le premier applique un pourcentage et le second réduit le total d'un montant brut.

Le coupon de pourcentage commence par P et réduit le panier de 10%. Exemple : PLILLYTWITCH  
Le coupon de réduction standard commande par S et réduit le panier de 50€. Exemple : SLILLYTWITCH

Implémenter le fonctionnement du coupon dans le panier en TDD.

Annexes
=======

SOLID
-----

Pour écrire du code plus facilement testable et maintenable, vous pouvez suivre les notions de [**SOLID**](https://fr.wikipedia.org/wiki/SOLID_(informatique)).  

Mes resources
-------------

Vous trouverez également d'autres liens vers des ressources complémentaires sur mon dépôt Gitlab : [Resources](https://gitlab.com/cours-iut/resources)
