import HeadComponent from 'next/head'
import CartComponent from '../components/cart/cart'
import CatalogComponent from '../components/items/catalog'
import FooterComponent from '../components/ui/footer'
import HeroComponent from '../components/ui/hero'
import Cart from '../domain/cart/cart'
import Database from '../domain/storage/database'
import useCart from '../hooks/use-cart'

const database = new Database();

export default function Home() {

  const items = database.getAllItems();
  const [cart, addItemToCart, removeItemFromCart, pay] = useCart(new Cart());

  return (
    <div className="bg-slate-100">
      <HeadComponent>
        <title>Keyboard Factory</title>
        <meta name="description" content="Keybard Factory Online Shop" />
        <link rel="icon" href="/favicon.ico" />
      </HeadComponent>

      <main className="grid grid-cols-12">
        <HeroComponent className="col-span-12">
          The Keyboard
          <br />
          Factory
        </HeroComponent>

        <CatalogComponent
          className="col-span-9"
          items={items}
          addToCart={(item) => addItemToCart(item)}
        />

        <CartComponent
          className="col-span-3 ml-4"
          cart={cart}
          removeFromCart={(item) => removeItemFromCart(item)}
          pay={() => pay()}
        />
      </main>

      <FooterComponent className="mt-8" />
    </div>
  )
}
