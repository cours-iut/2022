const availableCountries = ["fr", "eu", "us"]

const vat = {
    fr: 0.2,
    eu: 0.3,
    us: 0.5,
}

const shipment = {
    fr: 2.50,
    eu: 5.30,
    us: 12,
}

class Shipment {

    constructor(country = "fr") {
        if (!availableCountries.includes(country)) {
            throw Error("This country is not available yet")
        }

        this.vat = vat[country]
        this.shipment = shipment[country]
    }

    calculateShipment(amount) {
        return (amount * (1 + this.vat)) + this.shipment
    }

}

export { availableCountries as countries, vat, shipment }
export default Shipment