
export default class Item {

    constructor(id, image, price, name) {
        this.id = id;
        this.image = image;
        this.price = price;
        this.name = name;
    }

} 