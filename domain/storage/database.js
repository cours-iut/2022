import Item from '../item/item';

export default class Database {

    items = [];

    constructor() {
        this.items = shuffle([
            new Item("001", "kb_001.png", 150, "Planck"),
            new Item("002", "kb_002.png", 320, "Moonlander"),
            new Item("003", "kb_003.png", 130, "Iris"),
            new Item("005", "kb_005.png", 100, "XD75"),
            new Item("006", "kb_006.png", 105, "Orange Boy Ergo"),
            new Item("007", "kb_007.png", 110, "Game Roy Advance"),
            new Item("008", "kb_008.png", 210, "Stacked 1800"),
            new Item("009", "kb_009.png", 130, "Tofu 84"),
            new Item("010", "kb_010.png", 190, "Mode 65"),
            new Item("011", "kb_011.png", 125, "Corne Cherry"),
        ]);
    }

    getAllItems() {
        return this.items;
    }
}

function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * i);
        const k = array[i];
        array[i] = array[j];
        array[j] = k;
    }
    return array;
}
