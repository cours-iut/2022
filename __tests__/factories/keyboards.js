import Item from "../../domain/item/item"

const moonlander = () => new Item("moonlander", "moonlander.png", 320, "Moonlander Mark I")
const planck = () => new Item("planck", "planck.png", 250, "Planck")
const xd75 = () => new Item("xd75", "xd75.png", 110, "XD75")

export {
    moonlander, 
    planck,
    xd75,
}