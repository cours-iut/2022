import * as fc from "fast-check";
import Cart from "../../../domain/cart/cart";
import Item from "../../../domain/item/item";

test("empty cart returns 0", () => {
    // Arrange
    const cart = new Cart();

    // Act
    const total = cart.total;

    // Assert
    expect(total).toEqual(0);
})

test("cart with one item returns this item's price", () => {
    // Arrange
    const cart = new Cart([
        new Item("1", "1.png", 100, "Keyboard 1"),
    ]);

    // Act
    const total = cart.total;

    // Assert
    expect(total).toEqual(100);
});

test("cart with items return the sum of their prices", () => {
    // Arrange
    const cart = new Cart([
        new Item("1", "1.png", 100, "Keyboard 1"),
        new Item("2", "2.png", 200, "Keyboard 2"),
    ]);

    // Act
    const total = cart.total;

    // Assert
    expect(total).toEqual(300);
});

test("cart with items return the sum of their prices (random)", () => {
    fc.assert(
        fc.property(
            fc.integer({ min: 0 }),
            fc.integer({ min: 0 }),
            (priceA, priceB) => {
                // Arrange
                const cart = new Cart([
                    new Item("1", "1.png", priceA, "Keyboard 1"),
                    new Item("2", "2.png", priceB, "Keyboard 2"),
                ]);

                // Act
                const total = cart.total;

                // Assert
                expect(total).toEqual(priceA + priceB);
            }
        )
    )
});