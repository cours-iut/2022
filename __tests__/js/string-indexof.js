
test("when searched string is found returns its index", ()=>{
    // Arrange
    const helloWorld = 'hello world';

    // Act
    const index = helloWorld.indexOf('he');

    // Assert
    expect(index).toEqual(0);
});

test("when searched string is not found returns -1", ()=>{
    // Arrange
    const helloWorld = 'hello world';

    // Act
    const index = helloWorld.indexOf('hi');

    // Assert
    expect(index).toEqual(-1);
});

test("when searched string is found returns its index", ()=>{
    // Arrange
    const helloWorld = 'hello world';

    // Act
    const index = helloWorld.indexOf('wo');

    // Assert
    expect(index).toEqual(6);
});

test("when searched string is found muliple times, returns its first occurence's index", ()=>{
    // Arrange
    const helloWorld = 'hello world';

    // Act
    const index = helloWorld.indexOf('o');

    // Assert
    expect(index).toEqual(4);
});

test("when searched string is empty returns 0", ()=>{
    // Arrange
    const helloWorld = 'hello world';

    // Act
    const index = helloWorld.indexOf('');

    // Assert
    expect(index).toEqual(0);
});