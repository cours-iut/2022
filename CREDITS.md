Credits
=======

* Repository icon : [Freepik](https://www.freepik.com/)
* Keyboard Factory favicon : [Freepik](https://www.freepik.com/)
* Keyboard Pictures :
  * [35boi](https://www.reddit.com/user/35boi/) (Planck)
  * [myG0sh](https://www.reddit.com/user/myG0sh) (Moonlander)
  * [DustVoice](https://www.reddit.com/user/DustVoice/) (Iris)
  * [flaminpuffcornandsza](https://www.reddit.com/user/flaminpuffcornandsza/) (XD75)
  * [Jhaik](https://www.reddit.com/user/Jhaik/) (Orange Boy Ergo)
  * [ROYMEETSWORLD](https://www.reddit.com/user/ROYMEETSW0RLD/) (Game Roy Advance)
  * [CloysterDitch](https://www.reddit.com/user/CloysterDitch/) (Stacked 1800)
  * [TranquilMarmot](https://www.reddit.com/user/TranquilMarmot/) (Tofu84)
  * [XHolyPuffX](https://www.reddit.com/user/XHolyPuffX/) (Mode65)
  * [Ryukuro__](https://www.reddit.com/user/Ryukuro__/) (Corne Cherry V3)
