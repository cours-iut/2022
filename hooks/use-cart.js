import { useState } from "react";

export default function useCart(initialCart) {

  const [cart, setCart] = useState(initialCart);

  const addItemToCart = (item) => {
    setCart(cart.addItem(item));
  };

  const removeItemFromCart = (item) => {
    setCart(cart.removeItem(item));
  };

  const pay = async () => {
    await cart.pay();
    alert("Redirection to payment...");
  }

  return [cart, addItemToCart, removeItemFromCart, pay];
}