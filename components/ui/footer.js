
export default function Footer({className}) {
    return (
        <footer className={`${className} bg-slate-500 text-white p-4 grid grid-cols-5`}>
            <div></div>
            <div className="px-8 py-4">
                <h3 className="uppercase font-light mb-4">About the shop</h3>
                <p className="font-thin text-hyphen">
                    Tapi près d&apos;un milliard. Apercevant souvent bien des taches, sans y employer que de l&apos;eau-de-vie ; le bois des fenêtres.
                    Clouer des corbeaux à tous les adultes attendaient vainement de lui échapper.
                    Fais-y des chatouilles, des picotements sur la peau.
                </p>
            </div>
            <div className="px-8 py-4">
                <h3 className="uppercase font-light mb-4">Shop</h3>
                <ul>
                    <li><a href="#" className="font-thin">All products</a></li>
                    <li><a href="#" className="font-thin">Keyboards</a></li>
                </ul>
            </div>
            <div className="px-8 py-4">
                <h3 className="uppercase font-light mb-4">Informations</h3>
                <ul>
                    <li><a href="#" className="font-thin">Contact Us</a></li>
                    <li><a href="#" className="font-thin">About Us</a></li>
                    <li><a href="#" className="font-thin">FAQ</a></li>
                    <li><a href="#" className="font-thin">Our Blog</a></li>
                </ul>
            </div>
            <div></div>
        </footer>
    )
}