
export default function Hero({ className, children }) {
    return (
        <div className={`${className} h-96 header-image flex justify-center items-center`}>
            <h1 className="font-display text-center text-5xl font-bold text-slate-100 bg-slate-800/60 rounded py-6 px-8 flex-grow">
                {children}
            </h1>
        </div>
    )
}
