import CartItem from "./item";

export default function Cart({ className, cart, removeFromCart, pay}) {

    const content = cart.isEmpty() ? (
        EmptyCart()
    ) : (
        <div>
            <CartList items={cart.items} removeFromCart={removeFromCart} />
            <CartTotal total={cart.total} />
            <PayButton pay={() => pay()} />
        </div>
    );

    return (
        <div className={`${className} p-4 pr-8`}>
            <div className="pt-4 mb-4">
                <h2 className="text-3xl font-thin border-b border-b-slate-700">Your cart</h2>
            </div>
            {content}
        </div>
    )
}

function EmptyCart() {
    return (
        <p>Your cart is empty.</p>
    )
}

function CartList({ items, removeFromCart }) {
    return items.map((item, index) =>
        <CartItem
            key={index}
            className="my-2"
            item={item}
            removeFromCart={() => removeFromCart(item)}
        />
    )
}

function CartTotal({ total }) {
    return (
        <div className="flex flex-row items-baseline justify-between border-t border-t-slate-700 py-4 mt-8">
            <span className="text-xl flex-grow font-thin">
                Total
            </span>
            <span className="font-thin">{total} €</span>
        </div>
    )
}

function PayButton({ pay }) {
    return (
        <button className="py-2 px-4 font-bold text-white bg-teal-600 border border-teal-600 rounded w-full" onClick={() => pay()}>Payer</button>
    )
}