import Image from "next/image";

export default function CartItem({className, item, removeFromCart}) {
    return (
        <div className={`${className} flex flex-row items-center`}>
            <Image
                src={`/images/${item.image}`}
                alt={item.name}
                width={50}
                height={50}
            />
            <div className="flex-grow pl-6">
                <p className="font-thin uppercase">
                    {item.name}
                </p>
            </div>
            <div className="px-6">
                <p className="font-thin">
                    {item.price} €
                </p>
            </div>
            <button
                className="text-white border rounded border-red-700 bg-red-600 py-1 px-2 font-bold text-sm"
                onClick={removeFromCart}
            >
                X
            </button>
        </div>
    )
}