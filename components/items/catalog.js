import Item from './item';

export default function Catalog({ className, items, addToCart }) {
    return (
        <div className={`${className} flex flex-row flex-wrap`}>
            <div className="flex flex-row flex-wrap gap-4 mt-4">
                {items.map(item =>
                    <Item
                        key={item.id}
                        item={item}
                        addToCart={addToCart}
                    />
                )}
            </div>
        </div>
    )
}