import Image from "next/image"

export default function Item({ item, addToCart, ...attributes }) {
    return (
        <div {...attributes} className="min-h-64 w-64 p-6 flex flex-col items-center">
            <Image
                src={`/images/${item.image}`}
                alt={item.name}
                width={400}
                height={400}
            />
            <span className="mt-4 uppercase font-light">{item.name}</span>
            <span className="mt-2 mb-4 font-light text-gray-600">{item.price} €</span>
            <button
                className="py-2 px-4 rounded border border-slate-400 hover:bg-slate-400 font-bold "
                onClick={() => addToCart(item)}
            >
                Buy
            </button>
        </div>
    )
}